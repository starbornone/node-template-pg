module.exports = {
  development: {
    database: 'template-db',
    dialect: 'postgres',
    host: process.env.db_url || 'localhost',
    operatorsAliases: 0,
    password: process.env.db_pass || 'tinder.woollen.farther.stipple.tough.madest',
    pool: {
      max: 30,
      min: 0,
      acquire: 30000,
      idle: 10000
    },
    port: process.env.db_port || 5432,
    provider: 'pg',
    username: process.env.db_user || 'postgres'
  },
  test: {
    database: 'template-db-test',
    dialect: 'postgres',
    host: 'localhost',
    operatorsAliases: 0,
    password: 'tinder.woollen.farther.stipple.tough.madest',
    pool: {
      max: 30,
      min: 0,
      acquire: 30000,
      idle: 10000
    },
    port: 5432,
    provider: 'pg',
    username: 'postgres'
  }
}