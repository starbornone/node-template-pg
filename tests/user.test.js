const assert = require('chai').assert;
const request = require('supertest');
const { beforeEach } = require('mocha');
const models = require('../models');
const User = require('../models/index').User;
const constant = require('../constants/user');

const app = require('../app');

describe('User Tests', () => {

  beforeEach(async () => {
    await models.sequelize.sync({ force: true, logging: false });
    await User.create({
      avatar: '',
      email: 'admin@test.com',
      name: 'Test Admin',
      password: 'password',
      role: constant.ROLE.ADMINISTRATOR
    })
  })

  describe('POST /users/test', () => {
    it('should return a message string', (done) => {
      request(app)
        .get('/api/users/test')
        .set('Accept', 'application/json')
        .expect(200)
        .end(function (err, res) {
          if (err) {
            return done(err)
          }
          assert.equal(res.body.message, 'Users route works.')
          done()
        })
    })
  })

  describe('POST /users/register', () => {
    it('should return an error if the email is already in use', (done) => {
      request(app)
        .post('/api/users/register')
        .send({
          email: 'admin@test.com',
          name: 'Admin Test',
          password: 'password'
        })
        .set('Accept', 'application/json')
        .expect(400)
        .end(function (err, res) {
          if (err) {
            return done(err)
          }
          assert.equal(res.body.message, 'There is already an account with that email address.')
          done()
        })
    })
  })

  describe('POST /users/login', () => {
    it('should login an existing user and send back a bearer token', (done) => {
      request(app)
        .post('/api/users/login')
        .send({
          email: 'admin@test.com',
          password: 'password'
        })
        .set('Accept', 'application/json')
        .expect(200)
        .end(function (err, res) {
          if (err) {
            return done(err)
          }
          assert.isString(res.body.token, 'token is a string')
          assert.include(res.body.token, 'Bearer', 'token is of bearer type')
          done()
        })
    })

    it('should login an existing user and send back a token expiry of 3600 seconds', (done) => {
      request(app)
        .post('/api/users/login')
        .send({
          email: 'admin@test.com',
          password: 'password'
        })
        .set('Accept', 'application/json')
        .expect(200)
        .end(function (err, res) {
          if (err) {
            return done(err)
          }
          assert.equal(res.body.tokenExpiry, 3600, 'token expiry is the number 3600')
          done()
        })
    })

    it('should login an existing user and send back user details', (done) => {
      request(app)
        .post('/api/users/login')
        .send({
          email: 'admin@test.com',
          password: 'password'
        })
        .set('Accept', 'application/json')
        .expect(200)
        .end(function (err, res) {
          if (err) {
            return done(err)
          }
          assert.isObject(res.body.user, 'user details are an object')
          assert.exists(res.body.user.email, 'user details include email')
          assert.exists(res.body.user.role, 'user details include role')
          done()
        })
    })

    it('should return an error for an invalid email', (done) => {
      request(app)
        .post('/api/users/login')
        .send({
          email: 'test@test.com',
          password: 'password'
        })
        .set('Accept', 'application/json')
        .expect(401)
        .end(function (err, res) {
          if (err) {
            return done(err)
          }
          assert.equal(res.body.message, 'We could not find an account with that email address.')
          done()
        })
    })

    it('should return an error for an invalid password', (done) => {
      request(app)
        .post('/api/users/login')
        .send({
          email: 'admin@test.com',
          password: 'passwordpassword'
        })
        .set('Accept', 'application/json')
        .expect(401)
        .end(function (err, res) {
          if (err) {
            return done(err)
          }
          assert.equal(res.body.message, 'The password is incorrect.')
          done()
        })
    })
  })

  describe('GET /users/list', () => {
    it('should return an array of users', (done) => {
      request(app)
        .get('/api/users/list')
        .set('Accept', 'application/json')
        .expect(200)
        .end(function (err, res) {
          if (err) {
            return done(err)
          }
          // insert assert on response
          done()
        })
    })
  })

})