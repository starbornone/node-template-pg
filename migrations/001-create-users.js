const constant = require('../constants/user');

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      avatar: {
        allowNull: true,
        type: Sequelize.STRING(250)
      },
      email: {
        allowNull: false,
        type: Sequelize.STRING(120)
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING(50)
      },
      password: {
        allowNull: false,
        type: Sequelize.STRING(100)
      },
      role: {
        allowNull: false,
        defaultValue: constant.ROLE.USER,
        type: Sequelize.ENUM,
        values: [
          constant.ROLE.ADMINISTRATOR,
          constant.ROLE.MODERATOR,
          constant.ROLE.USER
        ]
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }),
  down: (queryInterface) => queryInterface.dropTable('Users')
};