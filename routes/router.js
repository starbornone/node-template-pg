const boom = require('boom');

const index = require('./api/index');
const users = require('./api/users');

module.exports = app => {
  app.use('/api', index);
  app.use('/api/users', users);

  // catch 404 and forward to error handler
  app.use(function (req, res, next) {
    next(boom.notFound());
  });
}