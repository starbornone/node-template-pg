# Node Express Template

- [Node Express Template](#node-express-template)
- [Folder Structure](#folder-structure)
- [File & Folder Naming](#file--folder-naming)
- [Configuration](#configuration)
- [Demo Elements](#demo-elements)
- [Package Decisions/Notes](#package-decisionsnotes)
- [Appendix](#appendix)
  - [Included Packages](#included-packages)
    - [Required Packages](#required-packages)
    - [Optional Packages](#optional-packages)
    - [Development Packages](#development-packages)


`npm install -g sequelize sequelize-cli`

`npm install -g pg pg-hstore`

# Folder Structure

# File & Folder Naming

# Configuration

Some configuration is set up within the `config` folder. The usernames, passwords, and secrets here are generic things that we've generated. You will need to change these to something else and, preferably, use environment variables when possible. The values that you will want to change can be found as follows:

`config\config.js`:

```js
{
module.exports = {
  development: {
    database: 'template-db',
    password: process.env.db_pass || 'tinder.woollen.farther.stipple.tough.madest',
    port: process.env.db_port || 5432,
    username: process.env.db_user || 'postgres',
  },
  test: {
    database: 'template-db-test',
    password: 'tinder.woollen.farther.stipple.tough.madest',
    username: 'postgres'
  }
}
}
```

`config\keys.json`:

```json
{
  "secret": "sock.profile.peat.tipsy.boycott.juju"
}
```

# Demo Elements

# Package Decisions/Notes

# Appendix

## Included Packages

The following packages are included in this template.

### Required Packages

Required here in this context doesn't mean that a Node server cannot be created without these things, but rather that the way in which this particular project has been set-up, these packages are an integral part of that.

- **axios** ([GitHub page](https://github.com/axios/axios)): Promise based HTTP client for the browser and Node.
- **bcryptjs** ([GitHub page](https://github.com/dcodeIO/bcrypt.js)): Optimised bcrypt in JavaScript with zero dependencies.
- **body-parser** ([GitHub page](https://github.com/expressjs/body-parser)): Parses incoming request bodies in a middleware before your handlers, available under the req.body property.
- **boom** ([GitHub page](https://github.com/hapijs/boom)): Provides a set of utilities for returning HTTP errors.
- **cors** ([GitHub page](https://github.com/expressjs/cors)): Provides a Connect/Express middleware that can be used to enable CORS with various options.
- **express** ([GitHub page](https://github.com/expressjs/express)): Fast, unopinionated, minimalist web framework for Node.
- **express-validator** ([GitHub page](https://github.com/express-validator/express-validator)): An express.js middleware for validator.js.
- **joi** ([GitHub page](https://github.com/hapijs/joi)): Object schema description language and validator for JavaScript objects.
- **jsonwebtoken** ([GitHub page](https://github.com/auth0/Node-jsonwebtoken)): An implementation of JSON Web Tokens.
- **passport** ([GitHub page](https://github.com/jaredhanson/passport)): Authenticates requests, which it does through an extensible set of plugins known as strategies.
- **passport-jwt** ([GitHub page](https://github.com/themikenicholson/passport-jwt)): Passport strategy that lets you authenticate endpoints using a JSON web token, intended to be used to secure RESTful endpoints without sessions.
- **passport-local** ([GitHub page](https://github.com/jaredhanson/passport-local)): Passport strategy for authenticating using a username and password in your Node applications.
- **pg** ([GitHub page](https://github.com/brianc/Node-postgres)): Non-blocking PostgreSQL client for Node. Pure JavaScript and optional native libpq bindings.
- **pg-hstore** ([GitHub page](https://github.com/scarney81/pg-hstore)): A Node package for serializing and deserializing JSON data to hstore format.
- **sequelize** ([GitHub page](https://github.com/sequelize/sequelize)): A promise-based Node ORM for Postgres, MySQL, MariaDB, SQLite and Microsoft SQL Server. It features solid transaction support, relations, read replication and more.
- **sequelize-fixtures** ([GitHub page](https://github.com/domasx2/sequelize-fixtures)): A simple lib to load data to database using sequelize.

### Optional Packages

- **debug** ([GitHub page](https://github.com/visionmedia/debug)): A tiny JavaScript debugging utility modelled after Node core's debugging technique. Works in Node and web browsers.
- **gravatar** ([GitHub page](https://github.com/emerleite/node-gravatar)): A library to generate Gravatar URLs in Node Based on gravatar spec.
- **morgan** ([GitHub page](https://github.com/expressjs/morgan)): HTTP request logger middleware for Node.
- **pug** ([GitHub page](https://github.com/pugjs/pug)): A high-performance template engine heavily influenced by Haml and implemented with JavaScript for Node and browsers.
- **sequelize-cli** ([GitHub page](https://github.com/sequelize/cli)): The Sequelize Command Line Interface (CLI).

### Development Packages

- **chai** ([GitHub page](https://github.com/chaijs/chai)): A BDD/TDD assertion library for node and the browser that can be delightfully paired with any JavaScript testing framework.
- **eslint** ([GitHub page](https://github.com/eslint/eslint)): A tool for identifying and reporting on patterns found in ECMAScript/JavaScript code.
- **mocha** ([GitHub page](https://github.com/mochajs/mocha)): A feature-rich JavaScript test framework running on Node and in the browser, with tests that run serially, allowing for flexible and accurate reporting, while mapping uncaught exceptions to the correct test cases.
- **nodemon** ([GitHub page](https://github.com/remy/nodemon)): A tool that helps develop Node based applications by automatically restarting the node application when file changes in the directory are detected.
- **sinon** ([GitHub page](https://github.com/sinonjs/sinon)): A standalone and test framework agnostic JavaScript test spies, stubs and mocks.
- **supertest** ([GitHub page](https://github.com/visionmedia/supertest)): A uper-agent driven library for testing Node HTTP servers using a fluent API.