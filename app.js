const cors = require('cors');
const express = require('express');
const logger = require('morgan');
const passport = require('passport');
const path = require('path');

const router = require('./routes/router');
const errorMiddleware = require('./middleware/errorHandler');

const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));

app.use(cors());

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use(passport.initialize());

router(app);

app.use(errorMiddleware.errorHandler);

module.exports = app;
